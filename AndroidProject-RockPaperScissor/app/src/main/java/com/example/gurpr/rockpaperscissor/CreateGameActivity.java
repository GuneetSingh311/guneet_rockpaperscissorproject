package com.example.gurpr.rockpaperscissor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Random;

public class CreateGameActivity extends AppCompatActivity {

    TextView codeTV;
    DatabaseReference rootRef,gameRef;
    String secretcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_game);
        codeTV = findViewById(R.id.lblCode);
        codeGen();
    }

    public void codeGen(){

        ArrayList<String> codeAlpha = new ArrayList<>();

        codeAlpha.add("ASD");
        codeAlpha.add("DFG");
        codeAlpha.add("FGH");
        codeAlpha.add("PLK");
        codeAlpha.add("MNT");
        codeAlpha.add("QCU");

        ArrayList<String> codeNum = new ArrayList<>();

        codeNum.add("753");
        codeNum.add("214");
        codeNum.add("885");
        codeNum.add("552");
        codeNum.add("854");
        codeNum.add("697");

        String alpha = codeAlpha.get(new Random().nextInt(codeAlpha.size()));
        String num = codeNum.get(new Random().nextInt(codeNum.size()));

        codeTV.setText(alpha+num);
        Toast.makeText(this,alpha+num, Toast.LENGTH_SHORT).show();

        rootRef = FirebaseDatabase.getInstance().getReference();    //database reference pointing to root of database
        gameRef = rootRef.child("GAME");                            //database reference pointing to game node
        secretcode = alpha+num;
        gameRef.child("CODE").setValue(secretcode);
    }

    public void startGame(View view) {
        Intent intent = new Intent(CreateGameActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
