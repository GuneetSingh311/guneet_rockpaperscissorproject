package com.example.gurpr.rockpaperscissor;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class JoinGameActivity extends AppCompatActivity {

    Button join;
    EditText txtinput;
    TextView ghost;
    public String c ;
    DatabaseReference rootRef,gameRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_game);

        join = findViewById(R.id.btnJoin);
        txtinput = findViewById(R.id.txtJoinCode);
        ghost = findViewById(R.id.textView2);
        getCode();
    }

    public void join(View view) {

        if(TextUtils.isEmpty(txtinput.getText()))
        {
            Toast.makeText(this, "You did not enter a code", Toast.LENGTH_SHORT).show();
        }
        if(txtinput.getText().toString().toUpperCase().equals(ghost.getText())) {
            Intent intent = new Intent(JoinGameActivity.this, MainActivity.class);
            startActivity(intent);

        }
        else {
            txtinput.setText("");
            Toast.makeText(this, "You entered wrong code", Toast.LENGTH_SHORT).show();
        }
    }

    public void getCode(){
        rootRef = FirebaseDatabase.getInstance().getReference();
        gameRef = rootRef.child("GAME");
        gameRef.child("CODE").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String x = dataSnapshot.getValue(String.class);
                ghost.setText(x);
                //Log.d("Game Code is",c);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        //Toast.makeText(this, c, Toast.LENGTH_SHORT).show();
    }
}
