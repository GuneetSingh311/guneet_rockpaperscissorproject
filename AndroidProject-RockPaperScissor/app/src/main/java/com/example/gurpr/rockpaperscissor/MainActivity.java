package com.example.gurpr.rockpaperscissor;

import android.content.Intent;
import android.content.res.Resources;
import android.hardware.SensorManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.seismic.ShakeDetector;

import java.util.ArrayList;
import java.util.Random;



public class MainActivity extends AppCompatActivity implements ShakeDetector.Listener {

    ImageView image;
    int count = 0 ;
    int round = 0 ;
    boolean doubleBackToExitGame = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         image = findViewById(R.id.imgV);
        Toast.makeText(this,"Shake your phone twice to make a random selection.", Toast.LENGTH_LONG).show();
        SensorManager manager = (SensorManager) getSystemService(SENSOR_SERVICE);
        ShakeDetector detector = new ShakeDetector(this);
        detector.start(manager);
    }

    @Override
    public void hearShake() {
        count = count + 1;
        //Toast.makeText(this,"Shake Again", Toast.LENGTH_SHORT).show();

        if (count == 2){
            imgDis();
            count = 0;
            round = round + 1;

            // Store the score to Firebase according to the selections
            if (round == 3){
                Toast.makeText(this,"GAME FINISHED", Toast.LENGTH_SHORT).show();
                round = 0;
                // Move to scoreboard
                Intent intent = new Intent(MainActivity.this, ScoreActivity.class);
                startActivity(intent);
            }
        }
        else {}

    }

    public void imgDis(){
        ArrayList<String> img = new ArrayList<>();

        img.add("rock");
        img.add("paper");
        img.add("scissors");

        String imageName = img.get(new Random().nextInt(img.size()));
        Resources res = getResources();
        int resID = res.getIdentifier(imageName , "drawable", getPackageName());
        image.setImageResource(resID);
        System.out.println(imageName);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitGame) {
            super.onBackPressed();
            Intent intent = new Intent(MainActivity.this, MainScreenActivity.class);
            startActivity(intent);
            return;
        }

        this.doubleBackToExitGame = true;
        Toast.makeText(this, "Press BACK again to exit game", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitGame=false;
            }
        }, 2000);
    }
}

