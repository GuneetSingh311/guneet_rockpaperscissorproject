package com.example.gurpr.rockpaperscissor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
    }

    public void joinGame(View view) {
        Intent intent = new Intent(MainScreenActivity.this,
                JoinGameActivity.class);
        startActivity(intent);
    }

    public void createGame(View view) {
        Intent intent = new Intent(MainScreenActivity.this,
                CreateGameActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        finish();
        moveTaskToBack(true);
    }
}

