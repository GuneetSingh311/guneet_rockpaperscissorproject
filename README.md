#Rock Paper Scissor is a game which uses shake gestures and firebase cloud database to connect multiplayers and generate random choices.
#The one which generates a choice less than the other one gets eliminated from the game.
#The firebase database saves code which is used by other players to connect with each other and choice to save selected values.
#The one with superior choice wins the game.
![Alt text](https://bitbucket.org/GuneetSingh311/guneet_rockpaperscissorproject/downloads/1.PNG)
![Alt text](https://bitbucket.org/GuneetSingh311/guneet_rockpaperscissorproject/downloads/2.PNG)
![Alt text](https://bitbucket.org/GuneetSingh311/guneet_rockpaperscissorproject/downloads/3.PNG)
![Alt text](https://bitbucket.org/GuneetSingh311/guneet_rockpaperscissorproject/downloads/4.PNG)